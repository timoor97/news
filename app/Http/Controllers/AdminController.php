<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminauth');
        $this->middleware('can:admin');

    }
    public function index()
    {
        return view('admin.layouts.dashboard');
    }
}
