<?php

namespace App\Http\Controllers\AdminControllers;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequests\UnderCategory\CreateRequest;
use App\Http\Requests\AdminRequests\UnderCategory\UpdateRequest;
use App\Repositories\AdminRepository\UnderCategoryRepository;

class UnderCategoryController extends Controller
{
    protected $repository;

      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(UnderCategoryRepository $repository)
    {
         $this->middleware('permission:under-category-index', ['only' => ['index']]);
         $this->middleware('permission:under-category-create', ['only' => ['create','store']]);
         $this->middleware('permission:under-category-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:under-category-delete', ['only' => ['destroy']]);

         $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->repository->indexInterface();

        return view('admin.underCategory.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = $this->repository->createInterface();

        return view('admin.underCategory.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $under_category = $this->repository->storeInterface($request->all());

        return redirect()->route('admin.underCategory.index')
                        ->with('success','Category created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $under_category=$this->repository->showInterface($id);

        return view('admin.underCategory.show',compact('under_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        [$under_category,$category,$under_category_select] = $this->repository->editInterface($id);

        return view('admin.underCategory.edit',compact('under_category','category','under_category_select'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $under_category = $this->repository->updateInterface($request->all(), $id);

        return redirect()->route('admin.underCategory.index')
                        ->with('success','Category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->destroyInterface($id);

        return redirect()->route('admin.underCategory.index')
                        ->with('success','Category deleted successfully');
    }
}
