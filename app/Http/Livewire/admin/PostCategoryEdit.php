<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\Category;
class PostCategoryEdit extends Component
{
    public $category_id;
    public $categoryId;

    public function render()
    {
        $this->emit('selectCategory',$this->category_id);

        $category = Category::select('id','name')->get();

        if ($this->categoryId) {
            $categorySelected = Category::select('id','name')->where('id',$this->categoryId)->first();
        }

        return view('livewire.admin.post-category-edit',compact('category','categorySelected'));
    }
}
