<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\Category;
class PostCategory extends Component
{
    public $category_id;

    public function render()
    {
        $this->emit('selectCategory',$this->category_id);

        $category = Category::select('id','name')->get();

        return view('livewire.admin.post-category',compact('category'));
    }
}
