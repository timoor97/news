<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\AdminModels\Post;

class PostImageUpload extends Component
{
    public $image;
    public $post;
    protected $image_id;
    protected $post_id;

    protected $listeners = ['refreshData'];

    public function render()
    {

        $image=$this->image;

        return view('livewire.admin.post-image-upload',compact('image'));
    }

    public function imageId($image_id,$post_id)
    {
        $posta = Post::find($post_id);
        $posta->album->removeImage($image_id);
        $this->emit('refreshData');
    }

    public function refreshData()
    {
        $this->render();
    }
}
