<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\AdminModels\UnderCategory;
use App\Http\Livewire\admin\PostCategory;

class PostUnderCategory extends Component
{
    public $category_id;
    public $undercategory;

    protected $listeners = [
        'selectCategory' => 'selectUnderCategory',
    ];

    public function render()
    {
        if (!$this->undercategory) {
            $this->undercategory = UnderCategory::select('id','name')->get();
        }

        return view('livewire.admin.post-under-category',['undercategory' => $this->undercategory]);
    }

    public function selectUnderCategory($client_id)
    {
        $this->undercategory = UnderCategory::select('id','name')->where('category_id',$client_id)->get();
    }
}
