<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\AdminModels\UnderCategory;
use App\Http\Livewire\admin\PostCategoryEdit;

class PostUnderCategoryEdit extends Component
{
    public $categoryId;
    public $undercategory;
    public $underCategoryId;
    protected $underCategorySelected;

    protected $listeners = [
        'selectCategory' => 'selectUnderCategory',
    ];

    public function render()
    {
        if (!$this->undercategory) {

             $this->undercategory = UnderCategory::select('id','name','category_id')->where('id','!=',$this->underCategoryId)->where('category_id',$this->categoryId)->get();
             $this->underCategorySelected = UnderCategory::select('id','name','category_id')->where('id',$this->underCategoryId)->first();
        }

        return view('livewire.admin.post-under-category-edit',['undercategory' => $this->undercategory,'underCategoryId' => $this->underCategoryId ,'underCategorySelected' => $this->underCategorySelected]);
    }

    public function selectUnderCategory($cat_id)
    {
        $this->undercategory = UnderCategory::select('id','name','category_id')->where('category_id',$cat_id)->get();
        $this->underCategorySelected = '';
    }
}
