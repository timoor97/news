<?php

namespace App\Http\Requests\AdminRequests\Post;

use Illuminate\Foundation\Http\FormRequest;
class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_en'          => 'required|string|max:255',
            'images.*'            => 'mimes:jpg,jpeg|max:10240',
            'category_id'               => 'required',
            'under_category_id'               => 'required',
        ];
    }
}
