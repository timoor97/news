<?php

namespace App\Http\Requests\AdminRequests\Role;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Contracts\Validation\Validator;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:roles,name',
            'permission' => 'required',
        ];
    }

     /**
     * Find errors function
     *
     * @param Validator $v
     * @return void
     */

}
