<?php

namespace App\Repositories\AdminRepository;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\AdminInterfaces\RoleInterface;
use Illuminate\Database\Eloquent\Model;
use App\Models\AdminModels\Role;
use App\Models\AdminModels\Permission;
use Spatie\Translatable\HasTranslations;
use DB;
/**
 * Class RoleRepository.
 */
class RoleRepository implements RoleInterface
{
    /**
     * Model  variable
     *
     * @var [type]
     */
    protected $model;

    /**
     * Show Pages
     *
     * @var [type]
     */
    protected $per_page;

    /**
     * For initilizing
     *
     * @param Role $model
     */
    public function __construct(Role $model) {
        $this->model = $model;
        $this->per_page = request('per_page')?: 100000;
    }

    /**
     * @return string
     *  Return the model
     */
    public function indexInterface()
    {
        return  $this->model->paginate($this->per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function createInterface()
    {
        $permission = Permission::get();

        return $permission;
    }

     /**
     * show method find by id
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id)
    {
        $role = $this->model->find($id);

        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->get();


        return [$role, $rolePermissions];
    }

    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model
    {
        $role = $this->model->create(
            [
                'name'    => $data['name'],
                'slug'    => $data['name'],
                'description'    => ['en' => $data['description_en'], 'uz' => $data['description_uz']],
            ]
        );
        $role->syncPermissions($data['permission']);

        $role->save();

        return $role;
    }

     /**
     * Show the form for editing the specified resource
     *
     * @param [type] string
     * @return void
     */
    public function editInterface(int $id)
    {
        $role = $this->model->find($id);

        if(!$role){
            abort(404);
        }

        $permission = Permission::get();

        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        return [$role , $permission , $rolePermissions];
    }



    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id)
    {
        $role = $this->model->find($id);
        if(!$role){
            abort(404);
        }

        $role->name = $data['name'];

        $role->description=['en' => $data['description_en'], 'uz' => $data['description_uz']];

        $role->syncPermissions($data['permission']);

        $role->save();

        return $role;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     * @return void
     */
    public function destroyInterface(int $id)
    {
        $role = $this->model->find($id);
        if(!$role){
            abort(404);
        }
        $role->delete();

        return $role;

    }
}
