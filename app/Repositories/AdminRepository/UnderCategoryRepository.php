<?php

namespace App\Repositories\AdminRepository;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\AdminInterfaces\UnderCategoryInterface;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\UnderCategory;
use App\Models\AdminModels\Category;
use DB;
/**
 * Class RoleRepository.
 */
class UnderCategoryRepository implements UnderCategoryInterface
{
    /**
     * Model  variable
     *
     * @var [type]
     */
    protected $model;

    /**
     * Show Pages
     *
     * @var [type]
     */
    protected $per_page;

    /**
     * For initilizing
     *
     * @param Role $model
     */
    public function __construct(UnderCategory $model) {
        $this->model = $model;
        $this->per_page = request('per_page')?: 100000;
    }

    /**
     * @return string
     *  Return the model
     */
    public function indexInterface()
    {
        $data=$this->model->orderBy('id','DESC')->paginate($this->per_page);

        return $data;
    }


     /**
     * show method find by id
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id)
    {
        $under_category = $this->model->find($id);

        return $under_category;
    }

     /**
     * Show the form for creating a new resource.
     *
     */
    public function createInterface()
    {
        $category = Category::all()->pluck('name','id');

        return $category;
    }

    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model
    {
        $under_category = $this->model->create(
            [
                'name'    => ['en' => $data['under_category_en'], 'uz' => $data['under_category_uz']],
                'category_id'    => $data['category_id'],
            ]
        );

        $under_category->save();

        return $under_category;
    }

      /**
     * Show the form for editing the specified resource
     *
     * @param [type] string
     * @return void
     */
    public function editInterface(int $id)
    {
        $under_category = $this->model->find($id);



        if(!$under_category){
            abort(404);
        }


        $category = Category::pluck('name','id')->all();

        $under_category_select = DB::table("under_categories")->where("under_categories.id",$id)
        ->join('categories','under_categories.category_id','=','categories.id')
        ->pluck('categories.id','categories.name')
        ->all();

        return [$under_category,$category,$under_category_select];
    }
    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id)
    {
        $under_category = $this->model->find($id);

        $under_category->name=['en' => $data['under_category_en'], 'uz' => $data['under_category_uz']];
        $under_category->category_id=$data['category_id'];

        $under_category->save();

        return $under_category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     * @return void
     */
    public function destroyInterface(int $id)
    {
        $under_category = $this->model->find($id);

        if(!$under_category){
            abort(404);
        }
        $under_category->delete();

        return $under_category;

    }
}
