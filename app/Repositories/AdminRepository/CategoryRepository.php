<?php

namespace App\Repositories\AdminRepository;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\AdminInterfaces\CategoryInterface;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\Category;
use DB;
/**
 * Class RoleRepository.
 */
class CategoryRepository implements CategoryInterface
{
    /**
     * Model  variable
     *
     * @var [type]
     */
    protected $model;

    /**
     * Show Pages
     *
     * @var [type]
     */
    protected $per_page;

    /**
     * For initilizing
     *
     * @param Role $model
     */
    public function __construct(Category $model) {
        $this->model = $model;
        $this->per_page = request('per_page')?: 100000;
    }

    /**
     * @return string
     *  Return the model
     */
    public function indexInterface()
    {
        return  $this->model->orderBy('id','DESC')->paginate($this->per_page);
    }


     /**
     * show method find by id
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id)
    {
        $category = $this->model->find($id);

        return $category;
    }

    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model
    {
        $category = $this->model->create(
            [
                'name'    => ['en' => $data['category_en'], 'uz' => $data['category_uz']],
            ]
        );

        $category->save();

        return $category;
    }

      /**
     * Show the form for editing the specified resource
     *
     * @param [type] string
     * @return void
     */
    public function editInterface(int $id)
    {
        $category = $this->model->find($id);

        if(!$category){
            abort(404);
        }

        return $category;
    }
    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id)
    {
        $category = $this->model->find($id);

        $category->name=['en' => $data['category_en'], 'uz' => $data['category_uz']];

        $category->save();

        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     * @return void
     */
    public function destroyInterface(int $id)
    {
        $category = $this->model->find($id);

        if(!$category){
            abort(404);
        }
        $category->delete();

        return $category;

    }
}
