<?php

namespace App\Repositories\AdminRepository;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Interfaces\AdminInterfaces\PostInterface;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use App\Models\AdminModels\Post;
use App\Models\AdminModels\Role;
use App\Models\AdminModels\User;
use Illuminate\Support\Facades\Auth;
use DB;
/**
 * Class RoleRepository.
 */
class PostRepository implements PostInterface
{
    /**
     * Model  variable
     *
     * @var [type]
     */
    protected $model;

    /**
     * Show Pages
     *
     * @var [type]
     */
    protected $per_page;

    /**
     * For initilizing
     *
     * @param Role $model
     */
    public function __construct(Post $model) {
        $this->model = $model;
        $this->per_page = request('per_page')?: 100000;
    }

    /**
     * @return string
     *  Return the model
     */
    public function indexInterface()
    {

        $post=$this->model->orderBy('id','DESC')->paginate($this->per_page);
        $user = Auth::user()->hasRole('Moderator');

        return [$post,$user];
    }


     /**
     * show method find by id
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id)
    {
        $post = $this->model->find($id);

        return $post;
    }


    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model
    {
        $post = $this->model->create(
            [
                'name'    => ['en' => $data['post_en'], 'uz' => $data['post_uz']],
                'description'    => ['en' => $data['description_en'], 'uz' => $data['description_uz']],
                'category_id' => $data['category_id'],
                'under_category_id' => $data['under_category_id'],
                'user_id' => Auth::id(),
            ]
        );

        if(!empty($data['images'])) {
            $images=$data['images'];
            if ($data['images']) {
                foreach ($data['images'] as $key => $image) {
                    $post->album->addImage($image);
                }
            }
        }

        $post->save();

        return $post;
    }

      /**
     * Show the form for editing the specified resource
     *
     * @param [type] string
     * @return void
     */
    public function editInterface(int $id)
    {
        $post = $this->model->find($id);

        if(!$post){
            abort(404);
        }

        return $post;
    }
    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id)
    {
        $post = $this->model->find($id);

        $post->name=['en' => $data['post_en'], 'uz' => $data['post_uz']];
        $post->description=['en' => $data['description_en'], 'uz' => $data['description_uz']];
        $post->category_id=$data['category_id'];
        $post->under_category_id=$data['under_category_id'];

        if(!empty($data['images'])) {
            $images=$data['images'];
            if ($data['images']) {
                foreach ($data['images'] as $key => $image) {
                    $post->album->addImage($image);
                }
            }
        }

        $post->save();

        return $post;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param integer $id
     * @return void
     */
    public function destroyInterface(int $id)
    {
        $post = $this->model->find($id);

        if(!$post){
            abort(404);
        }
        $post->album->removeImage($id);
        $post->delete();

        return $post;

    }

}
