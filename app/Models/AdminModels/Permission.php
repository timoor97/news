<?php

namespace App\Models\AdminModels;

use Spatie\Permission\Models\Permission as SpatiePermission;
use Spatie\Translatable\HasTranslations;

class Permission extends SpatiePermission
{
    use HasTranslations;


    protected $table = 'permissions';
    public $translatable = [ 'description'];

    protected $fillable = ['name','slug','description', 'parent_id'];


    public function parent(){
        return $this->belongsTo(Permission::class);
    }
    public function children(){
        return $this->hasMany(Permission::class,'parent_id','id');
    }
}
