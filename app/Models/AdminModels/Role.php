<?php

namespace App\Models\AdminModels;

use Spatie\Permission\Models\Role as SpatieRole;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends SpatieRole
{
    use HasTranslations;
    use SoftDeletes;

    public $translatable = [ 'description'];

}
