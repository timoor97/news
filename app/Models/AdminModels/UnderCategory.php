<?php

namespace App\Models\AdminModels;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnderCategory extends Model
{
    use HasTranslations;
    use SoftDeletes;
    public $translatable = ['name'];

    protected $fillable = [
        'name', 'slug','category_id'
    ];

    public function category() {
        return $this->belongsTo('App\Models\AdminModels\Category','category_id');
    }
}
