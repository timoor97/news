<?php

namespace App\Models\AdminModels;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasTranslations;
    use SoftDeletes;

    public $translatable = ['name'];

    protected $fillable = [
        'name', 'slug'
    ];

    public function undercategory() {
        return $this->hasMany('App\Models\AdminModels\UnderCategory','category_id');
    }

}
