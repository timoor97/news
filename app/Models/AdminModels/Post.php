<?php

namespace App\Models\AdminModels;

use Bek96\Album\Traits\HasAlbum;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasTranslations;
    use HasAlbum;
    use SoftDeletes;

    public $translatable = ['name','description'];

    protected $fillable = [
        'name', 'description','status','user_id','category_id','under_category_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\AdminModels\User', 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\AdminModels\Category', 'category_id','id');
    }

    public function undercategory()
    {
        return $this->belongsTo('App\Models\AdminModels\UnderCategory', 'under_category_id','id');
    }

}
