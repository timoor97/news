<?php

namespace App\Interfaces\AdminInterfaces;
use Illuminate\Database\Eloquent\Model;

interface UnderCategoryInterface
{
    /**
     * Display a listing of the resource
     *
     * @return void
     */
    public function indexInterface();


    /**
     * Display the specified resource
     *
     * @param [type] $id
     * @return void
     */
    public function showInterface(int $id);


    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function createInterface();
    /**
     * Store a newly created resource in storage
     *
     * @param array $data
     * @return Model
     */
    public function storeInterface(array $data) :Model;

     /**
     * Show the form for editing the specified resource
     *
     * @param integer $id
     * @return void
     */
    public function editInterface(int $id);

    /**
     * Update the specified resource in storage
     *
     * @param array $data
     * @param integer $id
     * @return void
     */
    public function updateInterface(array $data, int $id);

    /**
     * Remove the specified resource from storage.
     *
     * @param [type] $id
     * @return void
     */
    public function destroyInterface(int $id);
}
