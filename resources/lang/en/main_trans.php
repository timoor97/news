<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Dashboard' => 'Dashboard',
    'Category' => 'Category',
    'UnderCategory' => 'UnderCategory',
    'Tag' => 'Tag',
    'Variant' => 'Variant',
    'Option' => 'Option',
    'post' => 'Post',
    'Role' => 'Role',
    'User' => 'User',
    'Data Table' => 'Data Table',
    'Show' => 'Show',
    'Search' => 'Search',
    'Name' => 'Name',
    'Guard name' => 'Guard name',
    'Description' => 'Description',
    'Actions' => 'Actions',
    'Edit' => 'Edit',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Create' => 'Create',
    'Settings' => 'Settings',
    'Email' => 'Email',
    'Permissions' => 'Permissions',
    'Submit' => 'Submit',
    'Back' => 'Back',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password',
    'Next' => 'Next',
    'Previous' => 'Previous',
    'First' => 'First',
    'Last' => 'Last',
    'Showing' => 'Showing',
    'To' => 'To',
    'Of' => 'Of',
    'Entries' => 'Entries',
    'Image' => 'Image',
    'Price' => 'Price',
    'Select' => 'Select',
    'Stock' => 'Stock',
    'Views' => 'Views',
    'Likes' => 'Likes',
    'Status' => 'Status',
    'Price' => 'Price',
    'must be select' => 'must be select',
    'must be required format and required size' => 'must be required format and required size',

];
