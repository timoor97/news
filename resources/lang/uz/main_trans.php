<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Dashboard' => 'Boshqaruv paneli',
    'Category' => 'Toifa',
    'UnderCategory' => 'Toifalarni toifasi',
    'Tag' => 'Yorliq',
    'Varyant' => 'Varyantlar',
    'Option' => 'Tahlash',
    'post' => 'Post',
    'Role' => 'Rol',
    'User' => 'Foydalanuvchi',
    'Data Table' => 'Malumotlar jadvali',
    'Show' => 'Korsatish',
    'Search' => 'Qidirmoq',
    'Name' => 'Ism',
    'Guard name' => 'Qoriqchi nomi',
    'Description' => 'Tavsif',
    'Actions' => 'Amallar',
    'Edit' => 'Tahrirlash',
    'Delete' => 'Ochirish',
    'Create' => 'Yaratish',
    'Settings' => 'Sozlamalar',
    'Email' => 'Elektron pochta',
    'Permissions' => 'Ruxsatlar',
    'Submit' => 'Topshirish',
    'Back' => 'Orqaga',
    'Password' => 'Parol',
    'Confirm Password' => 'Parolni tasdiqlang',
    'Next' => 'Keyingisi',
    'Previous' => 'Oldingi',
    'First' => 'Birinchi',
    'Last' => 'Oxirgi',
    'Last' => 'Oxirgi',
    'Showing' => 'Korsatilmoqda',
    'To' => 'Dan',
    'Of' => 'Gacha',
    'Entries' => 'Jami',
    'Image' => 'Rasm',
    'Price' => 'Narx',
    'Select' => 'Tanlang',
    'Stock' => 'Soni',
    'Views' => 'Korganlar soni',
    'Likes' => 'Yoqdi',
    'Status' => 'Holat',
    'Price' => 'Narx',
    'must be select' => 'tanlash kerak',
    'must be required format and required size' => 'talab qilingan formatda va hajmda bolishi kerak',
];
