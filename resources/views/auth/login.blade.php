@include('site.layouts.links')

<div class="main-content space-60">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Login</li>
        </ul>
        <div class="checkout-header">
            <ul>
                <li class="active">Login</li>
            </ul>
        </div>
        <!-- End checkout header -->
        <div class="account">
            <!-- End col-md-6 -->
            <div class="col-md-12 acc-login">
                <div class="title-product">
                    <h3>Login to your account</h3>
                </div>
                <!-- End product -->
                <div class="contact-form">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <label class=" control-label" for="inputemail2">Email</label>
                            <input type="email" class="form-control" placeholder="Email" id="email" name="email"
                                value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <!-- <input type="text" id="inputemail2" class="form-control" placeholder="Mark Stevens"> -->
                        </div>
                        <div class="form-group">
                            <label class=" control-label" for="inputpass2">Password</label>

                            <input type="password" class="form-control" placeholder="Password" id="password"
                                name="password" required autocomplete="current-password">

                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <!-- <input type="password" class="form-control" id="inputpass2" placeholder="******"> -->
                        </div>

                        <div class="form-group">
                        <a class="btn link-button acc" href="{{ route('register') }}" title="Login facebook">Register</a>
                        <!-- <button type="submit" class="btn link-button acc">Register</button> -->

                        <button type="submit" class="btn link-button acc">Sign in</button>
                        </div>

                        <!-- <div class="form-group">
                            <a class="btn link-button link-button-fb" href="#" title="Login facebook"><i
                                    class="fa fa-facebook"></i>Login facebook</a>
                            <a class="btn link-button link-button-tw" href="#" title="Login twitter"><i
                                    class="fa fa-twitter"></i>Login twitter</a>
                        </div> -->
                    </form>
                </div>
                <!-- End contact form -->
            </div>
        </div>
  @include('site.layouts.scripts')
