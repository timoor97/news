<div class="row form-group">
    <div class="col col-md-3"><label for="selectSm" class=" form-control-label">{{__('main_trans.Category')}}</label>
    </div>
    <div class="col-12 col-md-9">
        <select class="form-control-sm
                    form-control" required name="category_id" wire:model="category_id">
            <option value="">-- {{__('main_trans.Select')}} {{__('main_trans.Category')}} --</option>
            @if (is_array($category)|| is_object($category))
            @foreach($category as $item)
            <option value="{{ $item->id }}">
                {{ $item->name }}
            </option>
            @endforeach
            @endif


        </select>
    </div>
</div>
