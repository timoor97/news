<div class="row form-group">
    <div class="col col-md-3"><label for="selectSm" class=" form-control-label">{{__('main_trans.Category')}}</label>
    </div>
    <div class="col-12 col-md-9">
        <select class="form-control-sm
                    form-control" name="category_id" wire:model="category_id">
            @if (is_array($category)|| is_object($category))
            <option selected value="{{ $categorySelected->id }}">
                {{ $categorySelected->name }}
            </option>
            @foreach($category as $item)
            @if ($item->id!=$categorySelected->id)
            <option value="{{ $item->id }}">
                {{ $item->name }}
            </option>
            @endif
            @endforeach
            @endif
        </select>
    </div>
</div>
