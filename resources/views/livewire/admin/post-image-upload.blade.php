<div class="row form-group">
    <div class="col col-md-3"><label for="selectSm" class=" form-control-label">{{__('main_trans.Image')}}</label></div>
    <div class="col-12 col-md-9">

        @if ($image)
        @foreach ($image as $item)

        <div class="img-pos">
            <span wire:click="imageId({{ $item->id }},{{$post}})" class="close">&times;</span>
            <img class="primary_image" height="200px" width="200px" src="{{asset($item->path)}}" alt="" />
        </div>

        @endforeach
        @endif

        <input type="file" {{__('main_trans.Image')}} name="images[]" class="form-control-file" multiple>

        @if (count($errors)>0)
        <ul>
            <li style="color: red">{{__('main_trans.Image')}} {{__('main_trans.must be required format and required size')}}</li>
        </ul>
        @endif
    </div>
</div>
<style>
    .img-pos {
        position: relative;
    }

    .img-pos.close {
        position: absolute;
        top: 2px;
        right: 2px;
        z-index: 100;
    }

</style>
