<div class="row form-group">
    <div class="col col-md-3"><label for="selectSm"
            class=" form-control-label">{{__('main_trans.UnderCategory')}}</label>
    </div>
    <div class="col-12 col-md-9">
        <select class="form-control-sm
                    form-control" required  name="under_category_id">
            <option value="0">-- {{__('main_trans.Select')}} {{__('main_trans.UnderCategory')}} --</option>
            @if (is_array($undercategory)|| is_object($undercategory))
                @foreach($undercategory as $item)
                <option value="{{ $item->id }}">
                    {{ $item->name }}
                </option>
                @endforeach
            @endif
        </select>
    </div>
</div>
