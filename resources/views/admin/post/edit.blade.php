@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.post')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.post')}}/{{__('main_trans.Edit')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>{{__('main_trans.post')}}</strong> {{__('main_trans.Edit')}}
        </div>
        <div class="card-body card-block">
            {!! Form::model($post, ['method' => 'PUT','files'=>true,'route' => ['admin.post.update', $post->id]]) !!}
            <div class="row has-warning form-group">
                <div class="col col-md-3">
                    <label for="text-input" class=" form-control-label">{{__('main_trans.Name')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#en" role="tab"
                                aria-controls="home" aria-selected="true">En</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#uz" role="tab"
                                aria-controls="profile" aria-selected="false">Uz</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class=" tab-pane fade show active" id="en" role="tabpanel" aria-labelledby="home-tab">
                            <input value="{{$post->getTranslation('name', 'en')}}" required type="text" id="name"
                                name="post_en" placeholder="{{__('main_trans.Name')}}" class="form-control">
                        </div>
                        <div class=" tab-pane fade" id="uz" role="tabpanel" aria-labelledby="profile-tab">
                            <input value="{{$post->getTranslation('name', 'uz')}}" type="text" id="name"
                                name="post_uz" placeholder="{{__('main_trans.Name')}}" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="text-input" class=" form-control-label">{{__('main_trans.Description')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#end" role="tab"
                                aria-controls="home" aria-selected="true">En</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#uzd" role="tab"
                                aria-controls="profile" aria-selected="false">Uz</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="end" role="tabpanel" aria-labelledby="home-tab">
                            <input value="{{$post->getTranslation('description', 'en')}}" type="text"
                                id="description" name="description_en" placeholder="{{__('main_trans.Description')}}"
                                class="form-control">
                        </div>
                        <div class="tab-pane fade" id="uzd" role="tabpanel" aria-labelledby="profile-tab">
                            <input value="{{$post->getTranslation('description', 'uz')}}" type="text"
                                id="description" name="description_uz" placeholder="{{__('main_trans.Description')}}"
                                class="form-control">
                        </div>
                    </div>
                </div>
            </div>

            @livewire('admin.post-category-edit', ['categoryId' => $post->category_id])


            @livewire('admin.post-under-category-edit', ['underCategoryId' => $post->under_category_id,'categoryId' => $post->category_id])


            @livewire('admin.post-image-upload', ['image' => $post->album->images,'post' => $post->id])


            <button type="submit" class="btn btn-primary btn-sm">
                {{__('main_trans.Submit')}}
            </button>
            <a type="reset" class="btn btn-danger btn-sm"
                href="{{ route('admin.post.index') }}">{{__('main_trans.Back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
