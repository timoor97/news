@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.post')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.post')}}/{{__('main_trans.Show')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="content mt-3">
    <div class="animated fadeIn">
        <div class="pull-right" style="float:right">
            <a class="btn btn-primary" href="{{ route('admin.post.index') }}"> {{__('main_trans.Back')}}</a>
        </div>
        <div class="row">

            <div class="col-md-3">
                <div class="card">
                    @if ($post->album->images)
                    @foreach ($post->album->images as $item)
                    <img  class="card-img-top" src="{{asset($item->path)}}" alt="Card image cap" />
                    @endforeach
                    @endif
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ $post->name }}</h4>
                        <p class="card-text">{{ $post->description }}</p>
                    </div>
                </div>
            </div>


        </div><!-- .row -->
    </div><!-- .animated -->
</div><!-- .content -->

@endsection
