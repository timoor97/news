@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.Role')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.Role')}}/{{__('main_trans.Show')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="pull-right" style="float:right">
            <a class="btn btn-primary" href="{{ route('admin.roles.index') }}"> {{__('main_trans.Back')}}</a>
        </div>
        <div class="vue-lists">
            <div class="row">
                <div class="col-md-6">
                    <h3>{{ $role->name }}</h3>
                    <ul>
                        @if(!empty($rolePermissions))
                        @foreach($rolePermissions as $v)
                        <li class="label label-success"> {{ $v->description }}</li>
                        @endforeach
                        @endif
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
