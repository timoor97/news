@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.UnderCategory')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.UnderCategory')}}/{{__('main_trans.Create')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>{{__('main_trans.UnderCategory')}}</strong> {{__('main_trans.Create')}}
        </div>
        <div class="card-body card-block">
            {!! Form::open(array('route' => 'admin.underCategory.store','method'=>'POST')) !!}

            <div class="row form-group">
                <div class="col col-md-3">
                    <label for="text-input" class=" form-control-label">{{__('main_trans.Name')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#en" role="tab"
                                aria-controls="home" aria-selected="true">En</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#uz" role="tab"
                                aria-controls="profile" aria-selected="false">Uz</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="en" role="tabpanel" aria-labelledby="home-tab">
                             <input type="text" id="name" name="under_category_en">
                        </div>
                        <div class="tab-pane fade" id="uz" role="tabpanel" aria-labelledby="profile-tab">
                              <input type="text" id="name" name="under_category_uz">
                        </div>
                    </div>
                </div>
            </div>


            <div class="row form-group">
                <div class="col col-md-3"><label for="selectSm" class=" form-control-label">{{__('main_trans.Category')}}</label></div>
                <div class="col-12 col-md-9">
                    {!! Form::select('category_id', $category,[], array('class' => 'form-control-sm form-control')) !!}
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">
            {{__('main_trans.Submit')}}
            </button>
            <a type="reset" class="btn btn-danger btn-sm" href="{{ route('admin.underCategory.index') }}">{{__('main_trans.Back')}}</a>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection
