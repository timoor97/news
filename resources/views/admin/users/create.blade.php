@extends('admin.layouts.index')

@section('title')
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>{{__('main_trans.User')}}</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li class="active">{{__('main_trans.User')}}/{{__('main_trans.Create')}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <strong>{{__('main_trans.User')}}</strong> {{__('main_trans.Create')}}
        </div>
        <div class="card-body card-block">
            {!! Form::open(array('route' => 'admin.users.store','method'=>'POST')) !!}
            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label">{{__('main_trans.Name')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    {!! Form::text('name', null, array('placeholder' => __('main_trans.Name'),'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label">{{__('main_trans.Email')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    {!! Form::text('email', null, array('placeholder' => __('main_trans.Email'),'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label">{{__('main_trans.Password')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    {!! Form::password('password', array('placeholder' => __('main_trans.Password'),'class' => 'form-control')) !!}
                </div>
            </div>

            <div class="row form-group">
                <div class="col col-md-3"><label for="text-input" class=" form-control-label">{{__('main_trans.Confirm Password')}}</label>
                </div>
                <div class="col-12 col-md-9">
                    {!! Form::password('confirm-password', array('placeholder' => __('main_trans.Confirm Password'),'class' =>
                    'form-control')) !!}
                </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-3"><label for="selectSm" class=" form-control-label">{{__('main_trans.Role')}}</label></div>
                <div class="col-12 col-md-9">
                    {!! Form::select('roles[]', $roles,[], array('class' => 'form-control-sm form-control')) !!}
                </div>
            </div>

            <button type="submit" class="btn btn-primary btn-sm">
            {{__('main_trans.Submit')}}
            </button>
            <a type="reset" class="btn btn-danger btn-sm" href="{{ route('admin.users.index') }}">{{__('main_trans.Back')}}</a>
        </div>

        {!! Form::close() !!}
    </div>
</div>
</div>

@endsection
