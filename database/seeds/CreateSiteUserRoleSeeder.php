<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\AdminModels\Permission;
use Carbon\Carbon;

class CreateSiteUserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $checkRole = DB::table('roles')->where('slug', 'customer')->first();

        if (!$checkRole) {
            $roles = array(
                array(
                    "name" => "Customer",
                    "slug" => "customer",
                    "description" => json_encode([
                                        "uz"=>"Mijozlar uchun imkoniyatlar",
                                        "en"=>"Can access for Customer"
                                    ])
                ),
            );

            DB::table('roles')->insert($roles);

            $permissions = Permission::where('slug', 'customer')->first();

            $role = Role::where('slug', 'customer')->first();

            $role->permissions()->sync($permissions);
        }

    }
}
