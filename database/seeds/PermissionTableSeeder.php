<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\AdminModels\Permission;
use Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        ////////////////////////////////////////////////////////////////////////////
        /**
         * Web site User
         */
        $firstOrcreate = Permission::where('slug', 'customer')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "customer",
                "slug" => "customer",
                "description" => json_encode([
                    "uz"=>"Web Sayt Foydalanuvchilar uchun",
                    "en"=>"For Web Site Users"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
        }
        ////////////////////////////////////////////////////////////////////////////
        /**
         * Users
         */
        $firstOrcreate = Permission::where('slug', 'user-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "user-index",
                "slug" => "user-index",
                "description" => json_encode([
                    "uz"=>"Foydalanuvchi bo'limi",
                    "en"=>"User page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'user-index')->first();
            Permission::insert([
                "name" => "user-create",
                "slug" => "user-create",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchi qoshish imkonini",
                            "en"=>"Can access create user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-edit",
                "slug" => "user-edit",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchini ozgartirish imkoniyati",
                            "en"=>"Can access edit user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-show",
                "slug" => "user-show",
                "description" => json_encode([
                            "uz"=>"Foydalanuvchini korish imkoniyati",
                            "en"=>"Can access show user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "user-delete",
                "slug" => "user-delete",
                "description" => json_encode([
                            "uz"=>"Royhatdan otgan foydalanuvchini ochirish imkoni",
                            "en"=>"Can access delete user"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
         ////////////////////////////////////////////////////////////////////////////
        /**
         * Roles
         */
        $firstOrcreate = Permission::where('slug', 'role-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "role-index",
                "slug" => "role-index",
                "description" => json_encode([
                    "uz"=>"Rol bo'limi",
                    "en"=>"Role page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'role-index')->first();
            Permission::insert([
                "name" => "role-create",
                "slug" => "role-create",
                "description" => json_encode([
                            "uz"=>"Rol qoshish imkonini beradi",
                            "en"=>"Can access create role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-edit",
                "slug" => "role-edit",
                "description" => json_encode([
                            "uz"=>"Rolni ozgartirish imkoniyati",
                            "en"=>"Can access edit Role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-show",
                "slug" => "role-show",
                "description" => json_encode([
                            "uz"=>"Rolni korish imkoniyati",
                            "en"=>"Can access show role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "role-delete",
                "slug" => "role-delete",
                "description" => json_encode([
                            "uz"=>"Royhatdan otgan rolni ochirish imkoni",
                            "en"=>"Can access delete Role"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
        ////////////////////////////////////////////////////////////////////////////
        /**
         * Category
         */
        $firstOrcreate = Permission::where('slug', 'category-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "category-index",
                "slug" => "category-index",
                "description" => json_encode([
                    "uz"=>"Toifalar bo'limi",
                    "en"=>"Category page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'category-index')->first();
            Permission::insert([
                "name" => "category-create",
                "slug" => "category-create",
                "description" => json_encode([
                            "uz"=>"Toifa qoshish imkonini beradi",
                            "en"=>"Can access create category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "category-edit",
                "slug" => "category-edit",
                "description" => json_encode([
                            "uz"=>"Toifani ozgartirish imkoniyati",
                            "en"=>"Can access edit Category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "category-show",
                "slug" => "category-show",
                "description" => json_encode([
                            "uz"=>"Toifani korish imkoniyati",
                            "en"=>"Can access show category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "category-delete",
                "slug" => "category-delete",
                "description" => json_encode([
                            "uz"=>"Toifani ochirish imkoni",
                            "en"=>"Can access delete Category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
        ///////////////////////////////////////////////////////////////////

        /**
         * Under Category
         */
        $firstOrcreate = Permission::where('slug', 'under-category-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "under-category-index",
                "slug" => "under-category-index",
                "description" => json_encode([
                    "uz"=>"Toifalarni toifasi bo'limi",
                    "en"=>"Under Category page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'under-category-index')->first();
            Permission::insert([
                "name" => "under-category-create",
                "slug" => "under-category-create",
                "description" => json_encode([
                            "uz"=>"Toifalarga toifa qoshish imkonini beradi",
                            "en"=>"Can access create under category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "under-category-edit",
                "slug" => "under-category-edit",
                "description" => json_encode([
                            "uz"=>"Toifalarni toifasini ozgartirish imkoniyati",
                            "en"=>"Can access edit Under Category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "under-category-show",
                "slug" => "under-category-show",
                "description" => json_encode([
                            "uz"=>"Toifalarni toifasini korish imkoniyati",
                            "en"=>"Can access show under category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "under-category-delete",
                "slug" => "under-category-delete",
                "description" => json_encode([
                            "uz"=>"Toifani toifasini ochirish imkoni",
                            "en"=>"Can access delete Under Category"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
        ///////////////////////////////////////////////////////////////////


        /**
         * Post
         */
        $firstOrcreate = Permission::where('slug', 'post-index')->first();
        if(!$firstOrcreate){
            Permission::insert([
                "name" => "post-index",
                "slug" => "post-index",
                "description" => json_encode([
                    "uz"=>"Post bo'limi",
                    "en"=>"post page"
                ]),
                'created_at' => $now,
                'updated_at' => $now,
            ]);
            $parent = Permission::where('slug', 'post-index')->first();
            Permission::insert([
                "name" => "post-create",
                "slug" => "post-create",
                "description" => json_encode([
                            "uz"=>"Post qoshish imkonini beradi",
                            "en"=>"Can access create post"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "post-edit",
                "slug" => "post-edit",
                "description" => json_encode([
                            "uz"=>"Post ozgartirish imkoniyati",
                            "en"=>"Can access edit post"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "post-show",
                "slug" => "post-show",
                "description" => json_encode([
                            "uz"=>"Post korish imkoniyati",
                            "en"=>"Can access show post"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
            Permission::insert([
                "name" => "post-delete",
                "slug" => "post-delete",
                "description" => json_encode([
                            "uz"=>"Post ochirish imkoni",
                            "en"=>"Can access delete post"
                        ]),
                'created_at' => $now,
                'updated_at' => $now,
                'parent_id'  => $parent->id
            ]);
        }
        ///////////////////////////////////////////////////////////////////

    }
}
