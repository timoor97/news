<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']
    ], function(){


        Route::post('/user/logout', function () {
            Auth::logout();
        return redirect()->route('login');
        })->name('user-logout');

        Auth::routes();

        Route::get('/', 'AdminController@index')->name('admin.index');

        Route::group(['prefix' => 'admin'], function () {

            Route::get('/', 'AdminController@index')->name('admin.index');

            Route::get('/adminlogin', 'AdminControllers\AdminLoginController@index')->name('adminlogin');
            Route::post('/adminPostLogin', 'AdminControllers\AdminLoginController@login')->name('adminPostLogin');

            Route::prefix('roles')->group(function () {
                Route::get('/', 'AdminControllers\RoleController@index')->name('admin.roles.index');
                Route::get('/create', 'AdminControllers\RoleController@create')->name('admin.roles.create');
                Route::post('/store', 'AdminControllers\RoleController@store')->name('admin.roles.store');
                Route::get('/show/{id}', 'AdminControllers\RoleController@show')->name('admin.roles.show');
                Route::get('/edit/{id}', 'AdminControllers\RoleController@edit')->name('admin.roles.edit');
                Route::put('/update/{id}', 'AdminControllers\RoleController@update')->name('admin.roles.update');
                Route::delete('/destroy/{id}', 'AdminControllers\RoleController@destroy')->name('admin.roles.destroy');
            });

            Route::prefix('users')->group(function () {
                Route::get('/', 'AdminControllers\UserController@index')->name('admin.users.index');
                Route::get('/create', 'AdminControllers\UserController@create')->name('admin.users.create');
                Route::post('/store', 'AdminControllers\UserController@store')->name('admin.users.store');
                Route::get('/show/{id}', 'AdminControllers\UserController@show')->name('admin.users.show');
                Route::get('/edit/{id}', 'AdminControllers\UserController@edit')->name('admin.users.edit');
                Route::put('/update/{id}', 'AdminControllers\UserController@update')->name('admin.users.update');
                Route::delete('/destroy/{id}', 'AdminControllers\UserController@destroy')->name('admin.users.destroy');
            });


            Route::prefix('category')->group(function () {
                Route::get('/', 'AdminControllers\CategoryController@index')->name('admin.category.index');
                Route::get('/create', 'AdminControllers\CategoryController@create')->name('admin.category.create');
                Route::post('/store', 'AdminControllers\CategoryController@store')->name('admin.category.store');
                Route::get('/show/{id}', 'AdminControllers\CategoryController@show')->name('admin.category.show');
                Route::get('/edit/{id}', 'AdminControllers\CategoryController@edit')->name('admin.category.edit');
                Route::put('/update/{id}', 'AdminControllers\CategoryController@update')->name('admin.category.update');
                Route::delete('/destroy/{id}', 'AdminControllers\CategoryController@destroy')->name('admin.category.destroy');
            });


            Route::prefix('underCategory')->group(function () {
                Route::get('/', 'AdminControllers\UnderCategoryController@index')->name('admin.underCategory.index');
                Route::get('/create', 'AdminControllers\UnderCategoryController@create')->name('admin.underCategory.create');
                Route::post('/store', 'AdminControllers\UnderCategoryController@store')->name('admin.underCategory.store');
                Route::get('/show/{id}', 'AdminControllers\UnderCategoryController@show')->name('admin.underCategory.show');
                Route::get('/edit/{id}', 'AdminControllers\UnderCategoryController@edit')->name('admin.underCategory.edit');
                Route::put('/update/{id}', 'AdminControllers\UnderCategoryController@update')->name('admin.underCategory.update');
                Route::delete('/destroy/{id}', 'AdminControllers\UnderCategoryController@destroy')->name('admin.underCategory.destroy');
            });

            Route::prefix('post')->group(function () {
                Route::get('/', 'AdminControllers\PostController@index')->name('admin.post.index');
                Route::get('/create', 'AdminControllers\PostController@create')->name('admin.post.create');
                Route::post('/store', 'AdminControllers\PostController@store')->name('admin.post.store');
                Route::get('/show/{id}', 'AdminControllers\PostController@show')->name('admin.post.show');
                Route::get('/edit/{id}', 'AdminControllers\PostController@edit')->name('admin.post.edit');
                Route::put('/update/{id}', 'AdminControllers\PostController@update')->name('admin.post.update');
                Route::delete('/destroy/{id}', 'AdminControllers\PostController@destroy')->name('admin.post.destroy');
            });

        });

    });

